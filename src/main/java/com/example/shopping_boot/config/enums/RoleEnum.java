package com.example.shopping_boot.config.enums;

public enum RoleEnum {

    USER("1"),SUPER_ADMIN("2");

    private String id;

    private RoleEnum(String ID){
        this.id=ID;
    }

    public String getId(){
        return id;
    }
}
