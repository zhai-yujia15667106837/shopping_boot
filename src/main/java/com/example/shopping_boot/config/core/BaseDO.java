package com.example.shopping_boot.config.core;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 基础实体对象
 *
 * @author 欢欢源码
 */
@Data
public abstract class BaseDO implements Serializable {

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
//    /**
//     * 创建者，目前使用 SysUser 的 id 编号
//     *
//     * 使用 String 类型的原因是，未来可能会存在非数值的情况，留好拓展性。
//     */
//    @TableField(fill = FieldFill.INSERT)
//    private String creator;
//    /**
//     * 更新者，目前使用 SysUser 的 id 编号
//     *
//     * 使用 String 类型的原因是，未来可能会存在非数值的情况，留好拓展性。
//     */
//    @TableField(fill = FieldFill.INSERT_UPDATE)
//    private String updater;

    public Date getCreatedTime(Date date){
        return (Date) date.clone();
    }

    public Date getUpdateTime(Date date){
        return (Date) date.clone();
    }

    public void setCreatedTime(Date createdTime){
        this.createdTime = (Date) createdTime.clone();
    }

    public void setUpdateTime(Date updateTime){
        this.updateTime = (Date) updateTime.clone();
    }
}
