package com.example.shopping_boot.config.core;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "sa-token")
@Data
public class SaTokenConfig {

    //    私钥
    private String privateKey;

    //    公钥
    private String publicKey;

}
