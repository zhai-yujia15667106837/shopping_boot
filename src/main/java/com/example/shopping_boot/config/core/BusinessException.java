package com.example.shopping_boot.config.core;

public class BusinessException extends RuntimeException {
    /**
     * 错误编码
     */
    private R response;

    public BusinessException() {
        super();
    }

    public BusinessException(R response) {
        super();
        this.response = response;
    }

    public R getResponse() {
        return response;
    }

    public void setResponse(R response) {
        this.response = response;
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

    @Override
    public String toString() {
        return this.response + ":" + this.getMessage();
    }
}