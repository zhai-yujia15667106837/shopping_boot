package com.example.shopping_boot.config.exception;

import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.exception.NotPermissionException;
import cn.dev33.satoken.exception.NotRoleException;
import com.example.shopping_boot.config.core.BusinessException;
import com.example.shopping_boot.config.core.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Objects;

@Slf4j
@RestControllerAdvice
public class MyGlobalExceptionHandler {

    // 全局异常拦截（拦截项目中的所有异常）
    @ResponseBody
    @ExceptionHandler
    public R<String> handlerException(Exception e) {

        // 打印堆栈，以供调试
        e.printStackTrace();

        // 不同异常返回不同状态码
        if (e instanceof BindException) {
            BindException ee = (BindException) e;
            return R.failed("校验出现错误：" + Objects.requireNonNull(ee.getFieldError()).getDefaultMessage());
        }else if (e instanceof RuntimeException) {    // 普通异常, 输出：500 + 异常信息
            return R.failed("错误信息：" + e.getLocalizedMessage());
        } else if (e instanceof NotLoginException) {
            NotLoginException ee = (NotLoginException) e;
            return R.restResult(null, 10010, ee.getMessage());
        } else if (e instanceof NotRoleException) {        // 如果是角色异常
            NotRoleException ee = (NotRoleException) e;
            return R.failed("当前登录用户无此角色：" + ee.getRole());
        } else if (e instanceof NotPermissionException) {    // 如果是权限异常
            NotPermissionException ee = (NotPermissionException) e;
            return R.failed("当前登录用户无此权限：" + ee.getPermission());
        } else if (e instanceof BindException) {
            BindException ee = (BindException) e;
            return R.failed("校验出现错误：" + Objects.requireNonNull(ee.getFieldError()).getDefaultMessage());
        } else {
            return R.failed("服务器异常");
        }
        // 返回给前端
    }

    // 全局异常拦截（拦截项目中的NotLoginException异常）
    @ExceptionHandler(NotLoginException.class)
    public R handlerNotLoginException(NotLoginException nle)
            throws Exception {

        // 打印堆栈，以供调试
        nle.printStackTrace();

        // 判断场景值，定制化异常信息
        String message = "";
        if (nle.getType().equals(NotLoginException.NOT_TOKEN)) {
            message = "未提供token";
        } else if (nle.getType().equals(NotLoginException.INVALID_TOKEN)) {
            message = "token无效";
        } else if (nle.getType().equals(NotLoginException.TOKEN_TIMEOUT)) {
            message = "token已过期";
        } else if (nle.getType().equals(NotLoginException.BE_REPLACED)) {
            message = "token已被顶下线";
        } else if (nle.getType().equals(NotLoginException.KICK_OUT)) {
            message = "token已被踢下线";
        } else {
            message = "当前会话未登录";
        }

        // 返回给前端
        return R.restResult(null, 10010, message);
    }


    // 全局异常拦截（拦截项目中的NotLoginException异常）
    @ExceptionHandler(BusinessException.class)
    public R handlerBusinessException(BusinessException nle) {

        // 打印堆栈，以供调试
        nle.printStackTrace();
        // 返回给前端
        return nle.getResponse();
    }

}
