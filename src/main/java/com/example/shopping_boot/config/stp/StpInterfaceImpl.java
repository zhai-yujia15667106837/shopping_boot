package com.example.shopping_boot.config.stp;

import cn.dev33.satoken.stp.StpInterface;
import com.example.shopping_boot.pojo.entity.SysPermissionDO;
import com.example.shopping_boot.pojo.entity.SysRoleDO;
import com.example.shopping_boot.service.ISysPermissionService;
import com.example.shopping_boot.service.ISysRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 自定义权限验证接口扩展
 */
@Component    // 保证此类被SpringBoot扫描，完成Sa-Token的自定义权限验证扩展
@RequiredArgsConstructor
public class StpInterfaceImpl implements StpInterface {

    private final ISysRoleService isysRoleService;

    private final ISysPermissionService isysPermissionService;

//    获取权限列表
    @Override
    public List<String> getPermissionList(Object o, String s) {
        List<SysPermissionDO> permissionList = isysPermissionService.getPermissionList(o, s);
        return permissionList.stream().map(SysPermissionDO::getPermissionValue).collect(Collectors.toList());
    }

//    获取角色列表
    @Override
    public List<String> getRoleList(Object o, String s) {
        List<SysRoleDO> roleListByUserId = isysRoleService.getRoleListByUserId(o, s);
        return roleListByUserId.stream().map(SysRoleDO::getRoleValue).collect(Collectors.toList());
    }
}