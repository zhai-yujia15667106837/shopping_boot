package com.example.shopping_boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.shopping_boot.config.enums.RoleEnum;
import com.example.shopping_boot.pojo.dto.SysUserDTO;
import com.example.shopping_boot.pojo.entity.SysUserDO;
import com.example.shopping_boot.pojo.vo.UserRoleVo;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author 翟羽佳
 * @since 2022-10-21
 */
public interface ISysUserService extends IService<SysUserDO> {

    // 用户登录
    String login(SysUserDTO SysUser);

    Boolean register(SysUserDTO sysUser);

    Boolean addUserRole(SysUserDO sysUserDO,RoleEnum roleEnum);
}
