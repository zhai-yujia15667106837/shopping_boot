package com.example.shopping_boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.shopping_boot.pojo.entity.SysPermissionDO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 翟羽佳
 * @since 2022-10-25
 */
public interface ISysPermissionService extends IService<SysPermissionDO> {

    List<SysPermissionDO> getPermissionList(Object o, String s);
}
