package com.example.shopping_boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.shopping_boot.pojo.entity.SysRoleDO;
import com.example.shopping_boot.pojo.vo.UserRoleOneVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 翟羽佳
 * @since 2022-10-25
 */
public interface ISysRoleService extends IService<SysRoleDO> {

    List<SysRoleDO> getRoleListByUserId(Object o, String s);

    List<UserRoleOneVO> getListUserRoleOne(String userId);

    Boolean updateUserRoleOne(List<UserRoleOneVO> userRoleOneVOList);
}
