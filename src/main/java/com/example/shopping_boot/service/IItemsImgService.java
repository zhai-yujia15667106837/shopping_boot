package com.example.shopping_boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.shopping_boot.pojo.entity.ItemsImgDO;

import java.util.List;

/**
 * <p>
 * 商品图片表 服务类
 * </p>
 *
 * @author 翟羽佳
 * @since 2022-10-23
 */
public interface IItemsImgService extends IService<ItemsImgDO> {

    List<ItemsImgDO> queryPicturesByProductId(String id);
}
