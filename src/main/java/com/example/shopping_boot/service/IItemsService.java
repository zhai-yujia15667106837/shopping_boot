package com.example.shopping_boot.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.shopping_boot.pojo.entity.ItemsDO;
import com.example.shopping_boot.pojo.vo.ItemsVO;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 翟羽佳
 * @since 2022-10-22
 */
public interface IItemsService extends IService<ItemsDO> {
    IPage<ItemsVO> pageItems(IPage<ItemsDO> page);

    boolean addShoppingCart(String itemsId);
}
