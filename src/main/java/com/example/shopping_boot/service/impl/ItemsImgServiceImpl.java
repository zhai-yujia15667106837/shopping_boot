package com.example.shopping_boot.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.shopping_boot.mapper.ItemsImgMapper;
import com.example.shopping_boot.pojo.entity.ItemsImgDO;
import com.example.shopping_boot.service.IItemsImgService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 商品图片表 服务实现类
 * </p>
 *
 * @author 翟羽佳
 * @since 2022-10-23
 */
@Service
public class ItemsImgServiceImpl extends ServiceImpl<ItemsImgMapper, ItemsImgDO> implements IItemsImgService {

    /**
     * 根据商品id查询商品图片
     * @param id
     * @return
     */
    @Override
    public List<ItemsImgDO> queryPicturesByProductId(String id) {
        return this.list(Wrappers.<ItemsImgDO>lambdaQuery().eq(ItemsImgDO::getItemId, id));
    }
}
