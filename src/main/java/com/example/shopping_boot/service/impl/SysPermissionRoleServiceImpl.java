package com.example.shopping_boot.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.shopping_boot.mapper.SysPermissionMapper;
import com.example.shopping_boot.mapper.SysPermissionRoleMapper;
import com.example.shopping_boot.pojo.entity.SysPermissionDO;
import com.example.shopping_boot.pojo.entity.SysPermissionRoleDO;
import com.example.shopping_boot.service.ISysPermissionRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 翟羽佳
 * @since 2022-10-25
 */
@Service
@RequiredArgsConstructor
public class SysPermissionRoleServiceImpl extends ServiceImpl<SysPermissionRoleMapper, SysPermissionRoleDO> implements ISysPermissionRoleService {

    private final SysPermissionMapper sysPermissionMapper;

    @Override
    public List<SysPermissionDO> getPermissionByRoleId(String roleId) {
        List<SysPermissionRoleDO> sysPermissionRoles = this.baseMapper.selectList(Wrappers.<SysPermissionRoleDO>lambdaQuery().eq(SysPermissionRoleDO::getRoleId, roleId));

        if (sysPermissionRoles.size()==0) {
            return new ArrayList<>();
        }

        return sysPermissionMapper.selectList(Wrappers.<SysPermissionDO>lambdaQuery()
                .in(SysPermissionDO::getId, sysPermissionRoles.stream().map(SysPermissionRoleDO::getPermissionId).toArray()));
    }
}
