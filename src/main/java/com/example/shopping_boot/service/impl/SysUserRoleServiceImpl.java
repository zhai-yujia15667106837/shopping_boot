package com.example.shopping_boot.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.shopping_boot.mapper.SysRoleMapper;
import com.example.shopping_boot.mapper.SysUserMapper;
import com.example.shopping_boot.mapper.SysUserRoleMapper;
import com.example.shopping_boot.pojo.entity.SysRoleDO;
import com.example.shopping_boot.pojo.entity.SysUserDO;
import com.example.shopping_boot.pojo.entity.SysUserRoleDO;
import com.example.shopping_boot.pojo.vo.UserRoleVo;
import com.example.shopping_boot.service.ISysUserRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 翟羽佳
 * @since 2022-10-25
 */
@Service
@RequiredArgsConstructor
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRoleDO> implements ISysUserRoleService {

    private final SysUserMapper sysUserMapper;

    private final SysRoleMapper sysRoleMapper;

    @Override
    public List<UserRoleVo> UserRoleAssociation() {
        List<SysUserDO> sysUserDOS = sysUserMapper.selectList(null);

        List<UserRoleVo> resUserRoleVo=new ArrayList<>();
        for (SysUserDO sysUserDO : sysUserDOS) {
            UserRoleVo userRoleVo=new UserRoleVo();

            userRoleVo.setId(sysUserDO.getId());
            userRoleVo.setName(sysUserDO.getName());
            List<SysUserRoleDO> sysUserRoleDOS = this.baseMapper.selectList(Wrappers.<SysUserRoleDO>lambdaQuery().eq(SysUserRoleDO::getUserId, sysUserDO.getId()));


            List<String> stringList=new ArrayList<>();
            for (SysUserRoleDO sysUserRoleDO : sysUserRoleDOS) {
                SysRoleDO sysRoleDO = sysRoleMapper.selectById(sysUserRoleDO.getRoleId());

                stringList.add(sysRoleDO.getRoleName());
            }

            userRoleVo.setRoleNames(stringList);


            resUserRoleVo.add(userRoleVo);
        }
        return resUserRoleVo;
    }
}
