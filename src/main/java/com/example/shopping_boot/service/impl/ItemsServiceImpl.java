package com.example.shopping_boot.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.shopping_boot.mapper.ItemsMapper;
import com.example.shopping_boot.mapstruct.ItemsConverter;
import com.example.shopping_boot.pojo.entity.ItemsDO;
import com.example.shopping_boot.pojo.vo.ItemsVO;
import com.example.shopping_boot.service.IItemsImgService;
import com.example.shopping_boot.service.IItemsService;
import com.example.shopping_boot.util.DataUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 翟羽佳
 * @since 2022-10-22
 */
@Service
@RequiredArgsConstructor
public class ItemsServiceImpl extends ServiceImpl<ItemsMapper, ItemsDO> implements IItemsService {

    private final DataUtil dataUtil;

    private final IItemsImgService itemsImgService;

    @Override
    public IPage<ItemsVO> pageItems(IPage<ItemsDO> page) {
        IPage<ItemsDO> pageItems = this.baseMapper.selectPage(page, Wrappers.<ItemsDO>lambdaQuery().eq(ItemsDO::getOnOffStatus, 1));

        List<ItemsVO> itemVos =new ArrayList<>();
        for (ItemsDO record : pageItems.getRecords()) {
            ItemsVO itemsVO = ItemsConverter.INSTANCE.ItemsDOToItemsVO(record);

            itemVos.add(itemsVO);
        }

        IPage<ItemsVO> responseItemsIPage = new Page<>();
        responseItemsIPage.setRecords(itemVos);
        responseItemsIPage.setTotal(pageItems.getTotal());
        responseItemsIPage.setSize(pageItems.getSize());
        responseItemsIPage.setCurrent(pageItems.getCurrent());
        responseItemsIPage.setPages(pageItems.getPages());
        return responseItemsIPage;
    }


    @Override
    public boolean addShoppingCart(String itemsId) {
        return true;
    }
}
