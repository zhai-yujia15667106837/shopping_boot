package com.example.shopping_boot.service.impl;

import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.shopping_boot.mapper.SysRoleMapper;
import com.example.shopping_boot.mapper.SysUserRoleMapper;
import com.example.shopping_boot.pojo.entity.SysRoleDO;
import com.example.shopping_boot.pojo.entity.SysUserRoleDO;
import com.example.shopping_boot.pojo.vo.UserRoleOneVO;
import com.example.shopping_boot.service.ISysRoleService;
import com.example.shopping_boot.util.RedisUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 翟羽佳
 * @since 2022-10-25
 */
@Service
@RequiredArgsConstructor
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRoleDO> implements ISysRoleService {

    private final SysUserRoleMapper sysUserRoleMapper;

    private final RedisUtil redisUtil;

    /**
     * tool 更改角色的时候调用清空缓存方法  暂未开发
     * @param o
     * @param s
     * @return
     */
    @Override
    public List<SysRoleDO> getRoleListByUserId(Object o, String s) {

        Object roleListRedis = redisUtil.getMapObject("RoleList", o);

        if (roleListRedis != null) {
            return (List<SysRoleDO>) roleListRedis;
        }

        List<SysUserRoleDO> sysUserRoles = sysUserRoleMapper.selectList(Wrappers.<SysUserRoleDO>lambdaQuery().eq(SysUserRoleDO::getUserId, o));
        if (sysUserRoles.size() == 0) {
            return new ArrayList<>();
        }
        List<SysRoleDO> roleList = this.baseMapper.selectList(Wrappers.<SysRoleDO>lambdaQuery().in(SysRoleDO::getId, sysUserRoles.stream().map(SysUserRoleDO::getRoleId).collect(Collectors.toList())));

        redisUtil.add("RoleList", MapUtil.of(o, roleList));
        return (List<SysRoleDO>) redisUtil.getMapObject("RoleList", o);
    }

    @Override
    public List<UserRoleOneVO> getListUserRoleOne(String userId) {
        List<SysRoleDO> sysRoleDOS = this.baseMapper.selectList(null);

        List<String> collect = sysUserRoleMapper.selectList(Wrappers.<SysUserRoleDO>lambdaQuery()
                .eq(SysUserRoleDO::getUserId, userId))
                .stream()
                .map(SysUserRoleDO::getRoleId).collect(Collectors.toList());

        List<UserRoleOneVO> userRoleOneVOList=new ArrayList<>();
        for (SysRoleDO sysRoleDO : sysRoleDOS) {
            UserRoleOneVO userRoleOneVO = new UserRoleOneVO();

            userRoleOneVO.setUserId(userId);
            userRoleOneVO.setRoleName(sysRoleDO.getRoleName());
            userRoleOneVO.setRoleValue(sysRoleDO.getRoleValue());
            userRoleOneVO.setRoleId(sysRoleDO.getId());

            userRoleOneVO.setIsMe(collect.contains(sysRoleDO.getId()));

            userRoleOneVOList.add(userRoleOneVO);
        }

        return userRoleOneVOList;
    }

    @Override
    public Boolean updateUserRoleOne(List<UserRoleOneVO> userRoleOneVOList) {
        sysUserRoleMapper.delete(Wrappers.<SysUserRoleDO>lambdaQuery().eq(SysUserRoleDO::getUserId, userRoleOneVOList.get(0).getUserId()));

        for (UserRoleOneVO userRoleOneVO : userRoleOneVOList.stream().filter(e -> e.getIsMe() == true).collect(Collectors.toList())) {
            sysUserRoleMapper.insert(SysUserRoleDO.builder().userId(userRoleOneVO.getUserId()).roleId(userRoleOneVO.getRoleId()).build());
        }

        redisUtil.delete("PermissionList", userRoleOneVOList.get(0).getUserId());
        redisUtil.delete("RoleList", userRoleOneVOList.get(0).getUserId());

        return true;
    }
}
