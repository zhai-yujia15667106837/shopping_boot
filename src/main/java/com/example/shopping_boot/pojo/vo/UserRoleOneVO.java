package com.example.shopping_boot.pojo.vo;

import lombok.Data;

@Data
public class UserRoleOneVO {

    private String userId;
    private String roleId;
    private String RoleName;
    private String RoleValue;

    private Boolean isMe;


}
