package com.example.shopping_boot.pojo.vo;

import lombok.Data;

import java.util.List;

@Data
public class ItemsVO {

    private String id;
    private String itemName;
    private String content;

    //图片
    private String imgUrl;
    private List<String> imgListUrl;

    //分类
    private String catId;
    private String catName;
    private String rootCatId;
    private String rootCatName;


    private String createdTime;
    private String updateTime;
}
