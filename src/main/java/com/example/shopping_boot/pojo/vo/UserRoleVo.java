package com.example.shopping_boot.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 *  用户_角色实体
 */
@Data
public class UserRoleVo implements Serializable {

    // 用户
    private String id;
    private String name;

    // 角色
    private List<String> roleNames;

//    private List<SysUserRole> userRoleVoList;

}
