package com.example.shopping_boot.pojo.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class SysUserDTO {
    /**
     * 名称
     */
    @NotNull(message = "名称不能为空")
    private String name;

    /**
     * 密码
     */
    @Length(min = 6,max = 12,message = "密码请输入在6-12位之间")
    private String password;

    /**
     * 手机
     */
    @Length(min = 6,max = 12,message = "手机号请输入在6-12位之间")
    private String phone;
}
