package com.example.shopping_boot.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.shopping_boot.config.core.BaseDO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author 翟羽佳
 * @since 2022-10-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("items")
public class ItemsDO extends BaseDO {

    private static final long serialVersionUID = 1L;

    /**
     * 商品id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 商品名称
     */
    private String itemName;

    /**
     * 分类id
     */
    private Integer catId;

    /**
     * 一级分类外键id
     */
    private Integer rootCatId;

    /**
     * 上下架状态，1：上架 2：下架
     */
    private Integer onOffStatus;

    /**
     * 商品内容
     */
    private String content;

}
