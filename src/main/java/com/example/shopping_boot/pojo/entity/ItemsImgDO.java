package com.example.shopping_boot.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.shopping_boot.config.core.BaseDO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 商品图片表
 * </p>
 *
 * @author 翟羽佳
 * @since 2022-10-23
 */
@TableName("items_img")
@Data
@EqualsAndHashCode(callSuper = true)
public class ItemsImgDO extends BaseDO {

    private static final long serialVersionUID = 1L;

    /**
     * 商品图片id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 商品外键id
     */
    private String itemId;

    /**
     * 图片地址
     */
    private String url;

    /**
     * 图片顺序
     */
    private Integer sort;

    /**
     * 是否主图，1：是，0：否
     */
    private Integer isMain;

}
