package com.example.shopping_boot.pojo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 翟羽佳
 * @since 2022-10-25
 */
@TableName("sys_permission_role")
@Data
public class SysPermissionRoleDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 权限id
     */
    private String permissionId;

    /**
     * 角色id
     */
    private String roleId;
}
