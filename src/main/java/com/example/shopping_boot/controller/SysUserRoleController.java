package com.example.shopping_boot.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.shopping_boot.config.core.R;
import com.example.shopping_boot.pojo.entity.ItemsDO;
import com.example.shopping_boot.service.ISysUserRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 翟羽佳
 * @since 2022-10-25
 */
@RestController
@RequestMapping("/shopping_boot/sysUserRole")
@RequiredArgsConstructor
public class SysUserRoleController {

    private final ISysUserRoleService iSysUserRoleService;

    @PostMapping("selectUserRole")
    @SaCheckPermission("select-user-role")
    public R selectUserRole(){
        return R.ok(iSysUserRoleService.UserRoleAssociation());
    }
}
