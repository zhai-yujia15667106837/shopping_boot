package com.example.shopping_boot.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.example.shopping_boot.config.core.R;
import com.example.shopping_boot.pojo.vo.UserRoleOneVO;
import com.example.shopping_boot.service.ISysRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 翟羽佳
 * @since 2022-10-25
 */
@RestController
@RequestMapping("/shopping_boot/sysRole")
@RequiredArgsConstructor
public class SysRoleController {

    private final ISysRoleService iSysRoleService;


    @GetMapping("ListUserRoleOne")
    @SaCheckPermission("list-user-role-one")
    public R ListUserRoleOne(String userId){
        return R.ok(iSysRoleService.getListUserRoleOne(userId));
    }

    @PostMapping("UpdateUserRoleOne")
    @SaCheckPermission("update-user-role-one")
    public R UpdateUserRoleOne(@RequestBody List<UserRoleOneVO> userList) {
        return R.ok(iSysRoleService.updateUserRoleOne(userList));
    }
}
