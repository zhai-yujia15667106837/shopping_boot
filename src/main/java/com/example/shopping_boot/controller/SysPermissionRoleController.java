package com.example.shopping_boot.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 翟羽佳
 * @since 2022-10-25
 */
@RestController
@RequestMapping("/shopping_boot/sysPermissionRole")
public class SysPermissionRoleController {

}
