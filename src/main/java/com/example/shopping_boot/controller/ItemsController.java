package com.example.shopping_boot.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaCheckRole;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.shopping_boot.config.core.R;
import com.example.shopping_boot.pojo.entity.ItemsDO;
import com.example.shopping_boot.service.IItemsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 翟羽佳
 * @since 2022-10-22
 */
@RestController
@RequestMapping("/shopping_boot/items")
@RequiredArgsConstructor
@SaCheckRole("super-admin")
public class ItemsController {
    private final IItemsService iItemsService;

    /**
     *  查询全部
     * @return 返回Items结果
     */
    @GetMapping("listItems")
    public R listItems(){
        return R.ok(iItemsService.list(),"处理成功");
    }

    @PostMapping("pageItems")
    public R pageItems(@RequestBody Page<ItemsDO> page){
        return R.ok(iItemsService.pageItems(page),"处理成功");
    }

    /**
     *  根据id修改
     * @return 返回Items结果
     */
    @PostMapping("updateItemsById")
    public R updateItemsById(@RequestBody @Valid ItemsDO items){
        return R.ok(iItemsService.updateById(items),"修改成功") ;
    }


    /**
     *  根据id删除
     * @return 返回Items结果
     */
    @GetMapping("deleteItemsById")
    public R deleteItemsById(String id){
        return R.ok(iItemsService.removeById(id),"删除成功") ;
    }

    /**
     * 添加商品
     *
     * @return 返回Items结果
     */
    @PostMapping("insertItems")
    public R insertUser(@RequestBody @Valid ItemsDO items) {
        return R.ok(iItemsService.save(items), "添加成功");
    }


    @PostMapping("updateItems2")
    @SaCheckPermission("update-items")
    public R updateItems2() {
        return R.ok("修改成功");
    }

    @PostMapping("superAdminTest")
    public R superAdminTest() {
        return R.ok("superAdminTest存在");
    }
}
