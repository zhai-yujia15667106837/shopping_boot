package com.example.shopping_boot.controller;

import com.example.shopping_boot.config.core.R;
import com.example.shopping_boot.pojo.entity.ItemsImgDO;
import com.example.shopping_boot.service.IItemsImgService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 商品图片表 前端控制器
 * </p>
 *
 * @author 翟羽佳
 * @since 2022-10-23
 */
@RestController
@RequestMapping("/shopping_boot/itemsImg")
@RequiredArgsConstructor
public class ItemsImgController {

    private final IItemsImgService iItemsImgService;

    /**
     * 查询所有的商品图片
     * @return
     */
    @GetMapping("listIItemsImg")
    public R listIItemsImg(){
        return R.ok(iItemsImgService.list(),"查询成功");
    }

    /**
     *  根据id修改
     * @return 返回ItemsImg结果
     */
    @PostMapping("updateListIItemsImgById")
    public R updateListIItemsImgById(@RequestBody @Valid ItemsImgDO itemsImg){

        return R.ok(iItemsImgService.updateById(itemsImg),"修改成功") ;
    }

    /**
     *  根据id删除
     * @return 返回ItemsImg结果
     */
    @GetMapping("deleteIItemsImgById")
    public R deleteIItemsImgById(String id){

        return R.ok(iItemsImgService.removeById(id),"删除成功") ;
    }
    /**
     *  增加
     * @return 返回ItemsImg结果
     */
    @PostMapping("insertIItemsImg")
    public R insertIItemsImg(@RequestBody @Valid ItemsImgDO itemsImg){

        return R.ok(iItemsImgService.save(itemsImg),"增加成功") ;
    }

}
