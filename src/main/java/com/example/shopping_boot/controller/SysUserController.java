package com.example.shopping_boot.controller;

import com.example.shopping_boot.config.core.R;
import com.example.shopping_boot.pojo.entity.SysUserDO;
import com.example.shopping_boot.service.ISysUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author 翟羽佳
 * @since 2022-10-21
 */
@RestController
@RequestMapping("/shopping_boot/sysUser")
@RequiredArgsConstructor
public class SysUserController {

    private final ISysUserService iSysUserService;

    /**
     *  查询全部
     * @return 返回userlist结果
     */
    @GetMapping("listUser")
    public R listUser(){
        return R.ok(iSysUserService.list(),"处理成功");
    }

    /**
     *  根据id修改
     * @return 返回userlist结果
     */
    @PostMapping("updateUserById")
    public R updateUser(@RequestBody @Valid SysUserDO sysUser){

        return R.ok(iSysUserService.updateById(sysUser),"修改成功") ;
    }


    /**
     *  根据id删除
     * @return 返回userlist结果
     */
    @GetMapping("deleteUser")
    public R deleteUser(String id){
        return R.ok(iSysUserService.removeById(id),"删除成功") ;
    }

//    /**
//     *  添加用户
//     * @return 返回userlist结果
//     */
//    @PostMapping("registerUser")
//    public R insertUser(@RequestBody @Valid SysUserDTO sysUser){
//        return R.ok(iSysUserService.register(sysUser),"成功");
//    }
//
//    /**
//     *  处理用户登录
//     * @return
//     */
//    @PostMapping("login")
//    public R login(@RequestBody @Valid SysUserDTO sysUser){
//        String token = iSysUserService.login(sysUser);
//
//        return R.ok(MapUtil.of("token", token));
//    }
}
