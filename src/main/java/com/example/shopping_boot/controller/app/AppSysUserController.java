package com.example.shopping_boot.controller.app;

import cn.hutool.core.map.MapUtil;
import com.example.shopping_boot.config.core.R;
import com.example.shopping_boot.pojo.dto.SysUserDTO;
import com.example.shopping_boot.service.ISysUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


/**
 * app端
 */
@RestController
@RequestMapping("/shopping_boot/App/sysUser")
@RequiredArgsConstructor
public class AppSysUserController {


    private final ISysUserService iSysUserService;


    /**
     *  注册
     * @return 返回userlist结果
     */
    @PostMapping("registerUser")
    public R insertUser(@RequestBody @Valid SysUserDTO sysUser){
        return R.ok(iSysUserService.register(sysUser),"成功");
    }

    /**
     *  用户登录
     * @return
     */
    @PostMapping("login")
    public R login(@RequestBody @Valid SysUserDTO sysUser){
        String token = iSysUserService.login(sysUser);

        return R.ok(MapUtil.of("token", token));
    }

}
