package com.example.shopping_boot.controller.app;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckRole;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.shopping_boot.config.core.R;
import com.example.shopping_boot.pojo.entity.ItemsDO;
import com.example.shopping_boot.service.IItemsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/shopping_boot/App/items")
@RequiredArgsConstructor
public class AppItemsController {

    private final IItemsService iItemsService;


    @PostMapping("pageItems")
    public R pageItems(@RequestBody Page<ItemsDO> page) {
        return R.ok(iItemsService.pageItems(page), "处理成功");
    }

    //    只有登录才可以进入此方法
    @GetMapping("addShoppingCart")
    @SaCheckLogin
    public R addShoppingCart(String itemsId) {
        return R.ok(iItemsService.addShoppingCart(itemsId), "添加成功");
    }

    @PostMapping("usertest")
    @SaCheckRole("user")
    public R userTest() {
        return R.ok("用户测试成功");
    }
}
