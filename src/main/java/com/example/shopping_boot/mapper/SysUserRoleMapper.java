package com.example.shopping_boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.shopping_boot.pojo.entity.SysUserRoleDO;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 翟羽佳
 * @since 2022-10-25
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRoleDO> {

}
