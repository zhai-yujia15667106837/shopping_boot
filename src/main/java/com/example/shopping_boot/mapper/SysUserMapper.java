package com.example.shopping_boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.shopping_boot.pojo.entity.SysUserDO;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author 翟羽佳
 * @since 2022-10-21
 */
public interface SysUserMapper extends BaseMapper<SysUserDO> {

}
