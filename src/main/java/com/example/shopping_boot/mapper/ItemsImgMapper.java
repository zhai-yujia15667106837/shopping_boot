package com.example.shopping_boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.shopping_boot.pojo.entity.ItemsImgDO;

/**
 * <p>
 * 商品图片表 Mapper 接口
 * </p>
 *
 * @author 翟羽佳
 * @since 2022-10-23
 */
public interface ItemsImgMapper extends BaseMapper<ItemsImgDO> {

}
