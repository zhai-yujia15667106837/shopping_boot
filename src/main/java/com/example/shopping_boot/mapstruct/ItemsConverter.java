package com.example.shopping_boot.mapstruct;

import com.example.shopping_boot.mapstruct.Decorator.ItemsDecorator;
import com.example.shopping_boot.mapstruct.tool.DateMapper;
import com.example.shopping_boot.pojo.entity.ItemsDO;
import com.example.shopping_boot.pojo.vo.ItemsVO;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {DateMapper.class})
@DecoratedWith(ItemsDecorator.class)
public interface ItemsConverter {

    ItemsConverter INSTANCE = Mappers.getMapper( ItemsConverter.class );

    ItemsVO ItemsDOToItemsVO(ItemsDO itemsDO);
}
