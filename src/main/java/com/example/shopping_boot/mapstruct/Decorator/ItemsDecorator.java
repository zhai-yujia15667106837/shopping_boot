package com.example.shopping_boot.mapstruct.Decorator;

import com.example.shopping_boot.mapstruct.ItemsConverter;
import com.example.shopping_boot.pojo.entity.ItemsDO;
import com.example.shopping_boot.pojo.entity.ItemsImgDO;
import com.example.shopping_boot.pojo.vo.ItemsVO;
import com.example.shopping_boot.service.IItemsImgService;
import com.example.shopping_boot.util.GetBeanUtil;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ItemsDecorator implements ItemsConverter {

    private final ItemsConverter itemsConverter;

    @Autowired
    public ItemsDecorator(ItemsConverter sysUserConverter) {
        this.itemsConverter = sysUserConverter;
    }

    private IItemsImgService iItemsImgService= GetBeanUtil.getBean(IItemsImgService.class);

    @Override
    public ItemsVO ItemsDOToItemsVO(ItemsDO itemsDO) {
        ItemsVO itemsVO = itemsConverter.ItemsDOToItemsVO(itemsDO);


//             图片信息
        List<ItemsImgDO> itemsImgs = iItemsImgService.queryPicturesByProductId(itemsVO.getId());
//            判断商品对应图片列表是否存在
        if (itemsImgs.size()>0){
//                判断主图是否存在
            if (itemsImgs.stream().filter(itemsImg -> itemsImg.getIsMain() == 1).count() > 0) {
//                    获取主图
                itemsVO.setImgUrl(itemsImgs.stream().filter(itemsImg -> itemsImg.getIsMain() == 1).findFirst().get().getUrl());
            } else {
//                    如果不存在主图，就取第一张图片
                itemsVO.setImgUrl(itemsImgs.get(0).getUrl());
            }

//                处理不是主图的列表
            List<String> imgUrls=new ArrayList<>();
//                获取不是主图的列表 是否存在
            if(itemsImgs.stream().filter(itemsImg -> itemsImg.getIsMain() == 0).count()>0){
//                    获取不是主图的列表放入imgUrls
                imgUrls= itemsImgs.stream().filter(itemsImg -> itemsImg.getIsMain() == 0).map(ItemsImgDO::getUrl).collect(Collectors.toList());
            }else{
//                    如果不存在，就取第一张图片
                imgUrls.add(itemsImgs.get(0).getUrl());
            }

            itemsVO.setImgListUrl(imgUrls);
        }

        return itemsVO;
    }
}
