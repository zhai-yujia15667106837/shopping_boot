package com.example.shopping_boot.mapstruct.Decorator;

import cn.dev33.satoken.secure.SaSecureUtil;
import com.example.shopping_boot.config.core.SaTokenConfig;
import com.example.shopping_boot.mapstruct.SysUserConverter;
import com.example.shopping_boot.pojo.dto.SysUserDTO;
import com.example.shopping_boot.pojo.entity.SysUserDO;
import com.example.shopping_boot.util.GetBeanUtil;
import org.springframework.beans.factory.annotation.Autowired;

public class SysUserDecorator implements SysUserConverter {

    private final SysUserConverter delegate;

    @Autowired
    public SysUserDecorator(SysUserConverter delegate) {
        this.delegate = delegate;
    }

    private SaTokenConfig saTokenConfig= GetBeanUtil.getBean(SaTokenConfig.class);


    // 密码加密
    public SysUserDO toDo(SysUserDTO sysUser) {
        SysUserDO sysUserDO = delegate.toDo(sysUser);
        sysUserDO.setPassword(SaSecureUtil.rsaEncryptByPublic(saTokenConfig.getPublicKey(), sysUserDO.getPassword()));

        return sysUserDO;
    }

    // 密码解密
    @Override
    public SysUserDTO toDto(SysUserDO sysUser) {
        SysUserDTO sysUserDTO = delegate.toDto(sysUser);

        sysUserDTO.setPassword(SaSecureUtil.rsaDecryptByPrivate(saTokenConfig.getPrivateKey(),sysUserDTO.getPassword()));

        return sysUserDTO;
    }
}
