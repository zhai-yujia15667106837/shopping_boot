package com.example.shopping_boot.mapstruct;

import com.example.shopping_boot.mapstruct.Decorator.SysUserDecorator;
import com.example.shopping_boot.mapstruct.tool.DateMapper;
import com.example.shopping_boot.pojo.dto.SysUserDTO;
import com.example.shopping_boot.pojo.entity.SysUserDO;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {DateMapper.class})
@DecoratedWith(SysUserDecorator.class)
public interface SysUserConverter {
    SysUserConverter INSTANCE = Mappers.getMapper(SysUserConverter.class);

    SysUserDO toDo(SysUserDTO sysUser);

    SysUserDTO toDto(SysUserDO sysUser);
}
