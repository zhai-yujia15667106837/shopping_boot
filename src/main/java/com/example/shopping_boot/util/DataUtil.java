package com.example.shopping_boot.util;

import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

@Component
public class DataUtil {
    public SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
}
