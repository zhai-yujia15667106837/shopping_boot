package com.example.shopping_boot;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;

@SpringBootTest
class ShoppingBootApplicationTests {

    @Test
    void contextLoads() {
    }
    @Test
    void CG() {
        FastAutoGenerator.create("jdbc:mysql://localhost:3306/shopping?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT%2B8&nullCatalogMeansCurrent=true&allowPublicKeyRetrieval=true",
                        "root", "password")
                .globalConfig(builder -> {
                    builder.author("翟羽佳") // 设置作者
//                            .enableSwagger() // 开启 swagger 模式
                            .fileOverride() // 覆盖已生成文件
                            .outputDir(System.getProperty("user.dir") + "/src/main/java"); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("com.example") // 设置父包名
                            .moduleName("shopping_boot") // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.xml, System.getProperty("user.dir")+"/src/main/resources/mapper")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder.addInclude("sys_permission_role") // 设置需要生成的表名
                    ; // 设置过滤表前缀
                })
//                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();

    }


    @Test
    public void test1(){
        char s1 = 'a';
        char s2 = 'Z';
        for (int i = 0; i < 26; i++) {
            System.out.println("小写："+s1+"大写："+s2);
            s1++;
            s2--;
        }

    }


    @Test
    public void test2(){
         double  m = 100000;
         int c = 0;
        while(true){
            if (m>50000){
                // a+=b   a=a+b
                m*=0.95; // m=m*0.95
               c++;
            }else if (m>1000){

                m-=1000; // m=m-1000
                c++;
            }else {
                break;
            }
        }
        System.err.println("可以过："+c+"路口");
    }
}
