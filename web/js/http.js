// import axios from 'axios'
// import {Message} from 'element-ui'

axios.defaults.baseURL='http://localhost:8090';//设置基址

// 请求超时时间
axios.defaults.timeout = 100000;

let base = '';

axios.interceptors.response.use(data=> {
    if(data.data.code!==undefined && data.data.code === 201){
        alert(data.data.msg);
        return;
    }
    if(data.data.code!==undefined && data.data.code === 500){
        alert("服务器错误!");
        return;
    }
    if(data.data.code!==undefined && data.data.code === 10010){
        alert(data.data.msg);

        localStorage.removeItem('token')
        //调转到login界面
        window.location.href = '/web/login/index.html'
        return;
    }
    return data.data;

}, err=> {
    // console.log("err获取：",err)
    if (err.response.status === 504||err.response.status === 404) {
        alert('服务器被吃了⊙﹏⊙∥');
    } else if (err.response.status === 403) {
        alert('权限不足,请联系管理员!');
    }else {
        alert('未知错误!');
    }
    return Promise.resolve(err);
})

// http request 拦截器
axios.interceptors.request.use(
    config => {
        if (localStorage.token) { //判断token是否存在
            config.headers.Authorization="Bearer "+localStorage.token;  //将token设置成请求头
        }
        return config;
    },
    err => {
        return Promise.reject(err);
    }
);

function  postRequest(url, params) {
    return axios({
        method: 'post',
        url: `${base}${url}`,
        data: params,
        headers: {
            'Content-Type': 'application/json;charset=UTF-8'
        }
    });
}
// export const uploadFileRequest = (url, params) => {
//     return axios({
//         method: 'post',
//         url: `${base}${url}`,
//         data: params,
//         headers: {
//             'Content-Type': 'multipart/form-data'
//         }
//     });
// }
// export const putRequest = (url, params) => {
//     return axios({
//         method: 'put',
//         url: `${base}${url}`,
//         data: JSON.parse(JSON.stringify(params)),
//         transformRequest: [function (data) {
//             let ret = ''
//             for (let it in data) {
//                 ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
//             }
//             return ret
//         }],
//         headers: {
//             'Content-Type': 'application/json;charset=UTF-8'
//         }
//     });
// }
// export const deleteRequest = (url) => {
//     return axios({
//         method: 'delete',
//         url: `${base}${url}`
//     });
// }
function getRequest(url) {
    return axios({
        method: 'get',
        url: `${base}${url}`
    });
}