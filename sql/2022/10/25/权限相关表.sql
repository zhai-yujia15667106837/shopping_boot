CREATE TABLE `sys_permission` (
                                  `id` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT 'id',
                                  `permission_name` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '权限名称',
                                  `permission_value` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '权限标识',
                                  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;




CREATE TABLE `sys_permission_role` (
                                       `id` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT 'id',
                                       `permission_id` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '权限id',
                                       `role_id` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '角色id',
                                       PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;


CREATE TABLE `sys_role` (
                            `id` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT 'id',
                            `role_name` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '角色名称',
                            `role_value` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '角色标识',
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;



CREATE TABLE `sys_user_role` (
                                 `id` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT 'id',
                                 `user_id` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '用户id',
                                 `role_id` varchar(255) COLLATE utf8mb4_bin NOT NULL COMMENT '角色id',
                                 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;