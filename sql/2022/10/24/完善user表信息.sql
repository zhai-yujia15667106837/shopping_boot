ALTER TABLE `shopping`.`sys_user`
    ADD COLUMN `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间' AFTER `email`,
ADD COLUMN `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间' AFTER `created_time`;