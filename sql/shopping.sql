/*
 Navicat Premium Data Transfer

 Source Server         : project
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : localhost:3306
 Source Schema         : shopping

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 24/10/2022 10:35:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for items
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '商品id',
  `item_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '商品名称',
  `cat_id` int(11) NOT NULL COMMENT '分类id',
  `root_cat_id` int(11) NOT NULL COMMENT '一级分类外键id',
  `on_off_status` int(255) NULL DEFAULT NULL COMMENT '上下架状态，1：上架 2：下架',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '商品内容',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of items
-- ----------------------------
INSERT INTO `items` VALUES ('1583660113744056322', '电脑', 3, 2, 1, '联想拯救者y9000', NULL, NULL);
INSERT INTO `items` VALUES ('1583662972598464514', '电脑', 3, 2, 1, '联想拯救者y9000', '2022-10-22 11:34:24', NULL);
INSERT INTO `items` VALUES ('1583663166425640961', '电脑', 3, 2, 1, 'asdas', '2022-10-22 11:34:57', '2022-10-22 11:35:33');
INSERT INTO `items` VALUES ('1583818869735825410', '手机', 3, 2, 1, '联想拯救者y9000', '2022-10-22 21:53:39', '2022-10-22 21:55:51');
INSERT INTO `items` VALUES ('1584189883535798274', 'iphone14ProMax', 4, 1, 1, '运行8G，畅通无阻', '2022-10-23 22:27:56', NULL);

-- ----------------------------
-- Table structure for items_img
-- ----------------------------
DROP TABLE IF EXISTS `items_img`;
CREATE TABLE `items_img`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '商品图片id',
  `item_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '商品外键id',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '图片地址',
  `sort` int(255) NULL DEFAULT NULL COMMENT '图片顺序',
  `is_main` int(255) NULL DEFAULT NULL COMMENT '是否主图，1：是，0：否',
  `created_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '商品图片表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of items_img
-- ----------------------------
INSERT INTO `items_img` VALUES ('1', '2', 'com.zhai', 1, 0, '2022-10-23 21:20:38', '2022-10-23 21:20:41');
INSERT INTO `items_img` VALUES ('1584195519833063426', '2', 'com.zhai', 2, 0, NULL, NULL);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'id',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '名称',
  `password` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '密码',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '手机',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1584027404864200705', '张三', '123123', '134688');
INSERT INTO `sys_user` VALUES ('1584186585541980161', '123', '123232', '123123');
INSERT INTO `sys_user` VALUES ('1584208973834928130', '123123123', '123222', '1222222');

SET FOREIGN_KEY_CHECKS = 1;
